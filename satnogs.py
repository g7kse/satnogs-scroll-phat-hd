#!/usr/bin/env python3

import urllib.parse
import requests
import scrollphathd
import time
from datetime import datetime

#Define the URL for the specific ground station. In this case ground station 33 (G7KSE VHF)
obs_url = 'https://network.satnogs.org/api/jobs/?id=&ground_station=33&satellite__norad_cat_id=&transmitter=&vetted_status=&vetted_user=&star$
# p.s. this is the base url obs_url = 'https://network.satnogs.org/api/jobs/?format=json'

#sort out what is needed from API and define the variables
json_data = requests.get(obs_url).json()
observation_data = json_data[0]
obs_id = observation_data['id']
start = observation_data['start']
sat = observation_data['tle0']
tx = observation_data['mode']

#Convert JSON date & time into useable string
date_time_obj = datetime.strptime(start, '%Y-%m-%dT%H:%M:%SZ')
date = date_time_obj.strftime("%d %b")
obtime = date_time_obj.strftime("%H:%M")

#rotate the scrollphat 180 degrees and set brightness
scrollphathd.rotate(degrees=180)
scrollphathd.set_brightness(0.5)

#print out the data
scrollphathd.write_string("...Next up: " + date + " " + obtime + " " + sat + " " + tx + ".....")
# should really replace this with an if/else in case nothing is scheduled
# then print using scrollphathd.write_string("...Nothing scheduled...."

# Auto scroll using a while + time mechanism (no thread)
while True:
    # Show the buffer
    scrollphathd.show()
    # Scroll the buffer content
    scrollphathd.scroll()
    # Wait for 0.1s
    time.sleep(0.05)
