# SatNOGS Scroll pHAT HD

A little python script that looks in the SatNOGS jobs API and returns the next obsrvation for a specific ground station (in my case 33)

The script is written in python and requires a Raspberry Pi zero w and Pimoroni scroll pHAT hd (there is a kit and that might be a better purchase - https://shop.pimoroni.com/products/scroll-bot-pi-zero-w-project-kit)

There are some useful instructions on how to install the required libraries at their github site (https://github.com/pimoroni/scroll-phat-hd) and a few examples to try out

The script takes the json api information and breaks it into a the useful components and extracts the date, time, satellite and transmitter for the next observation and then scrolls this across the display. A video of it working can be seen here

https://www.youtube.com/watch?v=y52NifqzmEI